# README #

The aim of this project is to build a powermeter for a bicycle. This device measures the power a rider produces when pushing the pedals in watts as well as the cadence and displays these parameters while riding. Powermeters are available for around 400 - 600€ on the market, but I am sure that it can be developed for much less than that.

### Goals ###

- Capture the power output of a cyclist with the use of strain gauge attached on the crank
- Measure the cadence with a gyroscope
- Show the data on a display attached to the handlebar

### Non-Goals ###

- Develop a head unit with GPS and elevation tracking
- Built a speedometer
- Save or upload the gathered data to a third party platform

## Directory-Structure ##

```
.
├── hardware                           : Everything related with hardware
│   ├── datasheets                     : Datasheets of the hardware
│   ├── kicad_crankUnit                : KiCad files of unit attached to the crank
│   └── kicad_handlebarUnit            : KiCad files of unit attached to the handlebar
└── sources                            : Source Code
```

## Links ##

In the following relevant links to information on the project and the software, as well as on related projects and demo videos can be found.

#### Related Projects ####

- https://www.instructables.com/id/Homemade-Cycling-Powermeter/
- https://hackaday.io/project/78037-bike-powermeter
