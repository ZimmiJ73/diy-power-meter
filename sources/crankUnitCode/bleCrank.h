#ifndef bleCrank_h
#define bleCrank_h

// INCLUDES //
#include <ArduinoBLE.h>
#include "led.h"

// FUNCTION HEADERS //
void BLEinitialiseCrank(void);
void BLEupdateCadenceCrank(ushort cad);
void BLEupdatePowerCrank(ushort pow);

#endif
