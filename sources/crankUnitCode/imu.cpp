#include "imu.h"

#define DEBUG 0

void IMUinitialise(void)
{
  if(!IMU.begin())
  {
    #if DEBUG >= 1
    Serial.println("failed to initialize IMU");
    #endif

    LEDerror();

    while(1);
  }

  #if DEBUG >= 1
  Serial.print("accelerometer sample rate = ");
  Serial.print(IMU.accelerationSampleRate());
  Serial.println(" Hz");
  Serial.println();
  Serial.println("acceleration in G's");
  Serial.println("X\tY\tZ");
  #endif
}
