#ifndef led_h
#define led_h

// INCLUDES //
#include <Arduino.h>

// DEFINES //
#define r 22u
#define g 23u
#define b 24u

// FUNCTION HEADERS //
void LEDinitialise(void);
void LEDerror(void);
void LEDconnecting(void);
void LEDworking(void);

#endif
