#include "bleCrank.h"

#define DEBUG 0

// BLE Cycling Speed and Cadence Service
BLEService cyclingSpeedAndCadenceService("1816");
// BLE Cycling Speed and Cadence Characteristic
BLEUnsignedShortCharacteristic CSCMeasurementChar("2A5B",  BLERead | BLENotify);

// BLE Cycling Power Service
BLEService cyclingPowerService("1818");
// BLE Cycling Power Measurement Characteristic
BLEUnsignedShortCharacteristic cyclingPowerMeasurementChar("2A63", BLERead | BLENotify);


void BLEinitialiseCrank(void)
{
  // begin initialization
  if (!BLE.begin())
  {
    #if DEBUG >= 1
    Serial.println("starting BLE failed");
    #endif

    LEDerror();
    while (1);
  }
  #if DEBUG >= 1
  Serial.println("starting BLE worked");
  #endif

  LEDconnecting();

  // set a device name for BLE device
  BLE.setDeviceName("CrankUnit");

  // set a local name for BLE device
  BLE.setLocalName("CrankUnit");

  // add cycling speed and cadence service and characteristic
  BLE.setAdvertisedService(cyclingSpeedAndCadenceService);
  cyclingSpeedAndCadenceService.addCharacteristic(CSCMeasurementChar);
  BLE.addService(cyclingSpeedAndCadenceService);
  if(!CSCMeasurementChar.writeValue(0))
  {
    #if DEBUG >= 1
    Serial.println("setting CSC value failed");
    #endif

    LEDerror();
  }

  // add cycling power service and characteristic
  BLE.setAdvertisedService(cyclingPowerService);
  cyclingPowerService.addCharacteristic(cyclingPowerMeasurementChar);
  BLE.addService(cyclingPowerService);
  if(!cyclingPowerMeasurementChar.writeValue(0))
  {
    #if DEBUG >= 1
    Serial.println("setting Power value failed");
    #endif

    LEDerror();
  }

  // start advertising BLE
  if(!BLE.advertise())
  {
    #if DEBUG >= 1
    Serial.println("BLE advertising failed");
    #endif

    LEDerror();
  }

  #if DEBUG >= 1
  Serial.println("bluetooth device active, waiting for connections...");
  #endif
}

void BLEupdateCadenceCrank(ushort cad)
{
  #if DEBUG >= 1
  Serial.print("cadence: ");
  Serial.println(cad);
  #endif

  // check if cycling speed and cadence characteristic is subscribed to
  if(CSCMeasurementChar.subscribed())
  {
    // update cadence characteristic
    if(!CSCMeasurementChar.writeValue(cad))
    {
      #if DEBUG >= 1
      Serial.println("setting CSC value failed");
      #endif

      LEDerror();
    }
  }
}

void BLEupdatePowerCrank(ushort pow)
{
  #if DEBUG >= 1
  Serial.print("power: ");
  Serial.println(pow);
  #endif

  // check if cycling power characteristic is subscribed to
  if(CSCMeasurementChar.subscribed())
  {
    // update cadence characteristic
    if(!cyclingPowerMeasurementChar.writeValue(pow))
    {
      #if DEBUG >= 1
      Serial.println("setting Power value failed");
      #endif

      LEDerror();
    }
  }
}
