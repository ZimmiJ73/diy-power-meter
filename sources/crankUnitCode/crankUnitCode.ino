#include "led.h"
#include "bleCrank.h"
#include "imu.h"

// >=1 for debug info on serial monitor; also change value in cpp file(s)
#define DEBUG 0

#define ADC_PIN A0
#define UPPER_ACCELERATION 1.20 // old 1.1
#define LOWER_ACCELERATION 0.80 // old 0.99
#define CRANK_LENGTH 17 // in mm
#define NEWTON_PER_VOLT_K 247
#define NEWTON_PER_VOLT_D -39

ushort cadence = 0;
ushort power = 0;

float x, y, z;
boolean oneQarter = false;
boolean halfTurn = false;
boolean threeQuarters = false;
boolean fullTurn = true;
unsigned long oldTime = 0;
unsigned long newTime = 0;
unsigned long turnTime = 0;
float adcVoltage = 0;
float avForce = 0;
float force = 0;
ushort samples = 0;
float turnSpeed = 0;

void setup()
{
  #if DEBUG >= 1
  Serial.begin(9600);
  while(!Serial);
  #endif

  // LED
  LEDinitialise();

  // ADC
  analogReadResolution(12);

  // IMU
  IMUinitialise();

  // BLE
  BLEinitialiseCrank();
}

void loop()
{
  // make device connectable and start advertising
  BLE.setConnectable(true);
  BLE.advertise();
  // wait for BLE central
  BLEDevice central = BLE.central();

  // if central is connected to the peripheral
  if(central)
  {
    #if DEBUG >= 1
    Serial.print("connected to central: ");
    Serial.println(central.address());
    #endif

    // make the device unconnectable when advertising and stop advertising
    BLE.setConnectable(false);
    BLE.stopAdvertise();

    LEDworking();

    // while the central is connected
    while (central.connected())
    {
      // CODE WHILE BLE CONNECTION IS RUNNING //

      // cadence/acceleration measurement
      if(IMU.accelerationAvailable())
      {
        IMU.readAcceleration(x, y, z);

        #if DEBUG >= 2
        Serial.print(x);
        Serial.print('\t');
        Serial.print(y);
        Serial.print('\t');
        Serial.println(z);
        #endif
      }

      // detect first quarter turn (x=-1)
      if(((x < -LOWER_ACCELERATION) && (x > -UPPER_ACCELERATION)) && (fullTurn))
      {
        #if DEBUG >= 1
        Serial.println(x);
        Serial.println("one quarter turn");
        #endif

        fullTurn = false;
        oneQarter = true;
      }

      // detect half turn (y=1)
      if(((y > LOWER_ACCELERATION) && (y < UPPER_ACCELERATION)) && (oneQarter))
      {
        #if DEBUG >= 1
        Serial.println(y);
        Serial.println("half turn");
        #endif

        oneQarter = false;
        halfTurn = true;
      }

      // detect third quarter turn (x=1)
      if(((x > LOWER_ACCELERATION) && (x < UPPER_ACCELERATION)) && (halfTurn))
      {
        #if DEBUG >= 1
        Serial.println(x);
        Serial.println("three quarters turn");
        #endif

        halfTurn = false;
        threeQuarters = true;
      }

      // detect full turn (y=-1)
      if(((y < -LOWER_ACCELERATION) && (y > -UPPER_ACCELERATION)) && (threeQuarters))
      {
        // FULL TURN

        #if DEBUG >= 1
        Serial.println(y);
        Serial.println("full turn");
        #endif

        // calculate cadence
        oldTime = newTime; // NOTE: does not work for first crank revolution
        newTime = millis();
        turnTime = newTime - oldTime;

        cadence = 60.0/(turnTime/1000.0);

        #if DEBUG >= 1
        Serial.print("cadence: ");
        Serial.println(cadence);
        #endif

        // calculate power
        turnSpeed = (2 * (CRANK_LENGTH/1000.0) * PI) / (turnTime/1000.0);
        power = 2 * avForce * turnSpeed;

        #if DEBUG >= 1
        Serial.print("power: ");
        Serial.println(power);
        #endif

        // send cadence and power
        BLEupdateCadenceCrank(cadence);
        BLEupdatePowerCrank(power);

        // reset values
        samples = 0;
        avForce = 0;

        threeQuarters = false;
        fullTurn = true;
      }

      // calculate force
      adcVoltage = analogRead(ADC_PIN) * (3.3 / 4095.0);
      force = (adcVoltage * NEWTON_PER_VOLT_K) + NEWTON_PER_VOLT_D;
      if (force >= 0 || true) // made contition always true
      {
        samples++;
        avForce = avForce - ((avForce - force) / samples);
      }

      #if DEBUG >= 1
      Serial.print("adcVoltage: ");
      Serial.println(adcVoltage);
      Serial.print("force: ");
      Serial.println(force);
      Serial.print("samples: ");
      Serial.println(samples);
      Serial.print("avForce: ");
      Serial.println(avForce);
      #endif

      // uncomment either to display these testing values on the LCD
      // xxx.yyy -> xxx in power field, yyy in cadence field
      // delay(100);
      // ADC Voltage
      // BLEupdatePowerCrank((int) adcVoltage);
      // BLEupdateCadenceCrank((adcVoltage - (int) adcVoltage) * 1000);
      // force
      // BLEupdatePowerCrank((int) force);
      // BLEupdateCadenceCrank((force - (int) force) * 1000);
    }

    #if DEBUG >= 1
    Serial.print("disconnected from central: ");
    Serial.println(central.address());
    #endif

    LEDconnecting();
  }
}
