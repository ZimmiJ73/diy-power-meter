#ifndef imu_h
#define imu_h

// INCLUDES //
#include <Arduino_LSM9DS1.h>
#include "led.h"

// FUNCTION HEADERS //
void IMUinitialise(void);

#endif
