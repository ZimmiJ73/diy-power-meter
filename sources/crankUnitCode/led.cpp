#include "led.h"

void LEDinitialise(void)
{
  pinMode(r, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(b, OUTPUT);
  digitalWrite(r, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(b, HIGH);
}

void LEDerror(void)
{
  digitalWrite(r, LOW);
  digitalWrite(g, HIGH);
  digitalWrite(b, HIGH);
}

void LEDconnecting(void)
{
  digitalWrite(r, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(b, LOW);
}

void LEDworking(void)
{
  digitalWrite(r, HIGH);
  digitalWrite(g, LOW);
  digitalWrite(b, HIGH);
}
