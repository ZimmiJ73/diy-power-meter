#ifndef bleHead_h
#define bleHead_h

// INCLUDES //
#include <ArduinoBLE.h>
#include "led.h"
#include "lcd.h"

// FUNCTION HEADERS //
void BLEinitialiseHead(void);
boolean BLEconnect(BLEDevice peri, BLECharacteristic* cscChar, BLECharacteristic* powerChar);

#endif
