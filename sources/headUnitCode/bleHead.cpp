#include "bleHead.h"

// >=1 for debug info on serial monitor; also change value in cpp file(s)
#define DEBUG 0

void BLEinitialiseHead(void)
{
  // begin BLE initialization
  if (!BLE.begin())
  {
    #if DEBUG >= 1
    Serial.println("starting BLE failed");
    #endif

    LEDerror();
    LCDprint("BLE Start Error");

    while(1);
  }
  #if DEBUG >= 1
  Serial.println("starting BLE worked");
  #endif

  LEDconnecting();

  // start scanning for peripherals
  BLE.scanForName("CrankUnit");

  #if DEBUG >= 1
  Serial.println("scanning for peripheral");
  #endif
}

boolean BLEconnect(BLEDevice peri, BLECharacteristic* cscChar, BLECharacteristic* powerChar)
{
  #if DEBUG >= 1
  // discovered peripheral: print out address, local name and advertised service
  Serial.print("found ");
  Serial.print(peri.address());
  Serial.print(" '");
  Serial.print(peri.localName());
  Serial.print("' ");
  Serial.print(peri.advertisedServiceUuid());
  Serial.println();
  #endif

  // check if peripheral name is correct
  if (peri.localName() != "CrankUnit")
  {
    LEDerror();
    LCDprint("Peri Name Error");

    return 0;
  }

  // stop scanning
  BLE.stopScan();

  // connect to the peripheral
  #if DEBUG >= 1
  Serial.println("connecting...");
  #endif

  LCDprint("Connecting...");

  if (peri.connect())
  {
    #if DEBUG >= 1
    Serial.println("connected");
    #endif
  } else
  {
    #if DEBUG >= 1
    Serial.println("failed to connect");
    #endif

    LEDerror();
    LCDprint("Connecting Error");

    return 0;
  }

  // discover peripheral attributes
  #if DEBUG >= 1
  Serial.println("discovering attributes...");
  #endif

  if (peri.discoverAttributes())
  {
    #if DEBUG >= 1
    Serial.println("attributes discovered");
    #endif

  } else
  {
    #if DEBUG >= 1
    Serial.println("attribute discovery failed");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("Attribute Error");

    return 0;
  }

  // retrieve BLE Cycling Speed and Cadence Characteristic
  *cscChar = peri.characteristic("2A5B");

  if (!cscChar)
  {
    #if DEBUG >= 1
    Serial.println("peripheral does not have CSC characteristic");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("CSC Char Error");

    return 0;
  } else if (!(*cscChar).canRead())
  {
    #if DEBUG >= 1
    Serial.println("peripheral does not have a readable CSC characteristic");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("CSC Readable Err");

    return 0;
  } else if (!(*cscChar).canSubscribe())
  {
    #if DEBUG >= 1
    Serial.println("peripheral does not have a subscribable CSC characteristic");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("CSC Subable Err");

    return 0;
  } else if (!(*cscChar).subscribe())
  {
    #if DEBUG >= 1
    Serial.println("subscribing to CSC characteristic failes");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("CSC Sub Error");

    return 0;
  }

  // retrieve BLE Cycling Power Characteristic
  *powerChar = peri.characteristic("2A63");

  if (!*powerChar)
  {
    #if DEBUG >= 1
    Serial.println("peripheral does not have power characteristic");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("Power Char Error");

    return 0;
  } else if (!(*powerChar).canRead())
  {
    #if DEBUG >= 1
    Serial.println("peripheral does not have a readable power characteristic");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("Pow Readable Err");

    return 0;
  } else if (!(*powerChar).canSubscribe())
  {
    #if DEBUG >= 1
    Serial.println("peripheral does not have a subscribable power characteristic");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("Pow Subable Err");

    return 0;
  } else if (!(*powerChar).subscribe())
  {
    #if DEBUG >= 1
    Serial.println("subscribing to power characteristic failes");
    #endif

    peri.disconnect();
    LEDerror();
    LCDprint("Pow Sub Error");

    return 0;
  }

  #if DEBUG >= 1
  Serial.println("discovered and subscribed to characteristics");
  #endif

  LCDprint("Connected");
  LEDworking();

  return 1;
}
