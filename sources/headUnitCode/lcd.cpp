#include "lcd.h"
LiquidCrystal lcd(RS, E, DB4, DB5, DB6, DB7);

void LCDinitialise(void)
{
  // 8, 2 instead of 16, 1 because of the used LCD
  lcd.begin(8, 2);
}

void LCDprint(char data[16])
{
  lcd.clear();

  for (uint8_t i = 0; i < strlen(data); i++)
  {
    // stop after max 16 signs
    if (i >= 16)
    {
      break;
    }

    lcd.write(data[i]);

    // after the first 7 signs the curser has to be reset to this position
    // because of the used LCD
    if (i == 7)
    {
      lcd.setCursor(40,0);
    }
  }
}
