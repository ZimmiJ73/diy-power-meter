#ifndef lcd_h
#define lcd_h

// INCLUDES //
#include <LiquidCrystal.h>

// DEFINES //
#define RS  12
// #define RW  11 // tied to GND
#define E   10
// #define DB0 9 // floating
// #define DB1 8 // floating
// #define DB2 7 // floating
// #define DB3 6 // floating
#define DB4 5
#define DB5 4
#define DB6 3
#define DB7 2

// FUNCTION HEADERS //
void LCDinitialise(void);
void LCDprint(char data[16]);

#endif
