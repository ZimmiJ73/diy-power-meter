#include <string.h>
#include "bleHead.h"
#include "lcd.h"
#include "led.h"

// >=1 for debug info on serial monitor; also change value in cpp file(s)
#define DEBUG 0

ushort power = 0;
ushort cadence = 0;
char lcdOutput[16];
BLECharacteristic cscCharacteristic;
BLECharacteristic powerCharacteristic;

void setup()
{
  #if DEBUG >= 1
  Serial.begin(9600);
  while(!Serial);
  #endif

  // LED
  LEDinitialise();

  // LCD
  LCDinitialise();

  // BLE
  BLEinitialiseHead();
  LCDprint("Searching...");
}

void loop()
{
  // check if a peripheral has been discovered
  BLEDevice peripheral = BLE.available();

  if(peripheral)
  {
    // try to connect to peripheral
    if(BLEconnect(peripheral, &cscCharacteristic, &powerCharacteristic))
    {
      cscCharacteristic.setEventHandler(BLEWritten, cscCharacteristicWritten);
      powerCharacteristic.setEventHandler(BLEWritten, powerCharacteristicWritten);

      // while the peripheral is connected
      while (peripheral.connected())
      {
        // CODE WHILE BLE CONNECTION IS RUNNING //
      }

      // peripheral disconnected, start scanning again
      #if DEBUG >= 1
      Serial.println("Peripheral disconnected");
      #endif

      LEDconnecting();
      LCDprint("Searching...");
      BLE.scanForName("CrankUnit");
    }
  }
}

void cscCharacteristicWritten(BLEDevice central, BLECharacteristic characteristic)
{
  cscCharacteristic.readValue(cadence);
  sprintf(lcdOutput, "%uW @ %urpm", power, cadence);
  LCDprint(lcdOutput);
}

void powerCharacteristicWritten(BLEDevice central, BLECharacteristic characteristic)
{
  powerCharacteristic.readValue(power);
  sprintf(lcdOutput, "%uW @ %urpm", power, cadence);
  LCDprint(lcdOutput);
}
